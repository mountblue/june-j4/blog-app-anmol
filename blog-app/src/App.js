import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import PostForm from "./components/PostForm";
import PostTitle from "./components/PostTitle";
import
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Happy Blogging</h1>
        </header>
        <PostForm />
        <hr/>
        <PostTitle />
      </div>
    );
  }
}

export default App;
