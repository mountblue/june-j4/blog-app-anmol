import React from "react";

const PostForm = () => {
  return (
    <div className="postForm-div">
      {/* <h1>Posts</h1> */}
      <form>
        <div className="input-div">
          <label>
            title: <input type="text" name="title" placeholder="enter a title"/>
          </label>
        </div>
        <div className="textarea-div">
          <label>
            description:
          </label>
          <textarea name="description" rows="5" placeholder="add a description"/>
        </div>
        <button type="submit">submit</button>
      </form>
    </div>
  );
};

export default PostForm;
